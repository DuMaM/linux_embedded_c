#!/bin/bash

source ./inc/colors.sh

ynFunc() {
    LOOP=1
    while [ $LOOP -eq 1 ]; do
        echo -e "${BLUE}$1 -> Continue[y/n]${NC}"
        read choice
        case "$choice" in 
            y|Y ) ${@:2}; LOOP=0;;
            n|N ) echo "Skipping"; LOOP=0;;
            * ) echo -e "${RED}Bad parameter - retring${NC}";;
        esac
    done    
}
