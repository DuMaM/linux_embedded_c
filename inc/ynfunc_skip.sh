#!/bin/bash

source ./inc/colors.sh

ynFunc() {
        echo -e "${BLUE}$1${NC}"
        ${@:2}
}
