#!/bin/bash

#TODO add skip parameter

cd "$(dirname "$0")"

YN_SKIP=0;
VERBOSE=0

show_help() {
    echo "This script helps you in installing and configuring most of popular embedded developer tools" 
    echo "Usage:" 
    echo "-h|? show this message"
    echo "-s skipping yes/no question and installing all possible features"
}

while getopts "h?vs" opt; do
    case "$opt" in
    h|\?|*)
        show_help
        exit 0
        ;;
    v)  VERBOSE=1
        ;;
    s)  YN_SKIP=1;
        ;;
    esac
done

if [ "$YN_SKIP" -eq 1 ]; then
    source ./inc/ynfunc_skip.sh
else
    source ./inc/ynfunc.sh
fi


ynFunc "Update repos?"                  sudo apt-get update && sudo apt-get upgrade && sudo apt-get dist-upgrade
ynFunc "Install version control"        sudo apt-get install subversion git git-cola gitk meld
ynFunc "Install compilation tools"      sudo apt-get install valgrind* linux-headers-$(uname -r) binutils binutils-dev binutils-multiarch binutils-multiarch-dev mtd-utils m4 gcc-arm-none-eabi gdb-multiarch build-essential
ynFunc "Install usb tools"              sudo apt-get install libusb-1.0-0 libusb-1.0-0-dev
ynFunc "Install parser tools"           sudo apt-get install libxml2*
ynFunc "Install file management tools"  sudo apt-get install binutils binutils-dev binutils-multiarch binutils-multiarch-dev mtd-utils m4
ynFunc "Add user into diallout group?"  sudo usermod -a -G dialout $USER
ynFunc "Load gitconfig file?"           cat inc/gitconfig >> ~/.gitconfig

exit 0;
